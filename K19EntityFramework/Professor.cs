﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K19EntityFramework
{
    public class Professor
    {
        public int ProfessorID { get; set; }
        public string Nome { get; set; }
        public Endereco Endereco { get; set; }
    }
}
